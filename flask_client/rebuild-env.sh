rm -rf env
virtualenv --python=python3.6 env
source env/bin/activate
pip install -r requirements.txt