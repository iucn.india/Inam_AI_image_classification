#!/bin/bash
DIRECTORY=$(cd `dirname $0` && pwd)
crontab $DIRECTORY/crontab-config
service cron start

cd $DIRECTORY
python app.py
