# Prerequisites:

## Install AWS CLI

https://aws.amazon.com/cli/

## Install Docker

https://docs.docker.com/install/

## Configure AWS CLI

To configure, type:

aws configure

### Details to be provided:

aws_access_key_id = AKIA24SPQYVRQ2ANOWCQ

aws_secret_access_key = 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

region = ap-south-1

Leave the output format as [None]

# Steps for building Docker Image and pushing it to AWS ECR

1. Clone IUCN repo in another build directory.

This is to ensure env folder and other temporary files dont make it to the docker image

2. Copy production models to the build directory

The location for the models is flask_client/models

3. Change the dockerfile to package code as part of image

        RUN mkdir /code

        # Uncomment the below line to package code with docker
        # ADD . /code

        WORKDIR /code


4. Build Docker Image 

        sudo docker build -t iucn-image .

    Test the built image once, to ensure the application is in working state (models in the appropriate place).

5. Upload Image to ECR

        sudo $(aws ecr get-login --no-include-email --region ap-south-1)

        sudo docker tag iucn-image:latest 748564825443.dkr.ecr.ap-south-1.amazonaws.com/iucn-image:latest

        sudo docker push 748564825443.dkr.ecr.ap-south-1.amazonaws.com/iucn-image:latest

