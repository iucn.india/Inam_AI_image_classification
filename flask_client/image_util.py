from PIL import Image
from datetime import datetime

def is_valid_image(img_file_path):
    try:
        img = Image.open(img_file_path) # open the image file
        img.verify() # verify that it is, in fact an image
        return True
    except (IOError, SyntaxError, ValueError) as e:
        print('Bad file:', img_file_path)
        print(e)
        return False

def get_image_file_date_time(img_file_path):
    try:
        exif = Image.open(img_file_path)._getexif()
    except AttributeError as e:
        print('Exif not found: ', img_file_path)
        return datetime.min

    if exif:
        if 36867 in exif:
            exif_tag = exif[36867]
            try:
                if '-' not in exif_tag:
                    current_image_date_time = datetime.strptime(exif_tag, '%Y:%m:%d %H:%M:%S')
                else:
                    current_image_date_time = datetime.strptime(exif_tag, '%Y-%m-%d %H:%M:%S')
                
                return current_image_date_time

            except ValueError as e:
                print('Bad Exif Format:', exif_tag)
                print(e)
                return datetime.min

    return datetime.min

