from util import get_image_info_list_from_folder, process_image_dataframe
from ml import save_as_csv, ml_config
import pandas as pd
import fire
import os

def predict_all_in_folder(base_dir = "/base_dir", learner_batchsize = 8):
    if not os.path.exists(base_dir):
        print("ERROR: The path doesn't exist: ", base_dir)
        return
    
    file_info_list = get_image_info_list_from_folder(base_dir)
    if not file_info_list:
        print("ERROR: No valid image files found. EXITING...")
        return

    image_dataframe: pd.DataFrame = pd.DataFrame(file_info_list)
    image_dataframe = process_image_dataframe(
            image_dataframe, learner_batchsize, ml_config, base_dir)

    csv_file_name = 'RESULTS.csv'
    save_as_csv(image_dataframe, base_dir, csv_file_name)

if __name__ == '__main__':
    fire.Fire(predict_all_in_folder)