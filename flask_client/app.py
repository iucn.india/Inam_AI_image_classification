import glob
# for reading operating system data
import os
# for regular expressions, saves time dealing with string data
import re
# system level operations (like loading files)
import sys
import uuid
from datetime import datetime, timedelta
from pathlib import Path

import pandas as pd
from flask import (Flask, Response, app, make_response, render_template,
                   request, send_from_directory, session)
from flask_httpauth import HTTPBasicAuth
from flask_uploads import (IMAGES, UploadSet, configure_uploads,
                           patch_request_class)
from imageio import imread, imsave
from PIL import Image
from werkzeug.security import check_password_hash, generate_password_hash

from cleanup import cleanup
from image_util import is_valid_image
from ml import ml_config, save_as_csv
from util import (convert_image_dataframe_to_results,
                  get_unique_filename_with_extn, get_unique_image_file_path,
                  process_image_dataframe)

# initalize our flask app
app = Flask(__name__)
app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'

auth = HTTPBasicAuth()

users = {
    "iucn": generate_password_hash("zentropy"),
    "zentropy": generate_password_hash("iucn")
}


@auth.verify_password
def verify_password(username, password):
    if username in users:
        return check_password_hash(users.get(username), password)
    return False
    

app.config['CSV_FOLDER'] = os.getcwd() + '/csv_result'

app.config['UPLOADED_PHOTOS_DEST'] = os.getcwd() + '/uploads'
app.config['PHOTOS_DEST'] = os.getcwd() + '/static/uploaded/'
app.config['LEARNER_BATCHSIZE'] = 2

photos = UploadSet('photos', IMAGES)
configure_uploads(app, photos)
patch_request_class(app, size=1024 * 1024 * 1024)


@app.before_request
def make_session_permanent():
    session.permanent = True
    app.permanent_session_lifetime = timedelta(minutes=1)


@app.route('/')
@auth.login_required
def index():
    # initModel()
    # render out pre-built HTML file right on the index page
    return render_template("index.html")


@app.route('/upload', methods=['GET', 'POST'])
@auth.login_required
def upload():
    if request.method == 'POST' and 'photo' in request.files:
        file_info_list = []
        for item in request.files.getlist('photo'):
            if item.filename == '':
                return render_template("index.html")
            filename = photos.save(item)
            uploaded_file_path = app.config['UPLOADED_PHOTOS_DEST'] + \
                '/' + filename

            if is_valid_image(uploaded_file_path):
                target_dir = app.config['PHOTOS_DEST']
                img_file_path, img_file_name = get_unique_image_file_path(
                    uploaded_file_path, target_dir)
                os.rename(uploaded_file_path, img_file_path)
                file_info = {'event_set': 'upload_set', 'event_set_prefix': None,
                             'img_file_path': img_file_path, 'photo_id': filename, 'enc_file_name': img_file_name}
                file_info_list.append(file_info)

        image_dataframe: pd.DataFrame = pd.DataFrame(file_info_list)
        image_dataframe = process_image_dataframe(
            image_dataframe, app.config['LEARNER_BATCHSIZE'], ml_config, target_dir)

        label_cols = list(ml_config['info'].keys())
        results = convert_image_dataframe_to_results(
            image_dataframe, label_cols)

        csv_file_name = get_unique_filename_with_extn('.csv')
        save_as_csv(image_dataframe, app.config['CSV_FOLDER'], csv_file_name)

        return render_template("index.html", results=results, csv_file_name=csv_file_name)

    return render_template("index.html")


@app.route('/download/<path:csv_file_name>', methods=['GET'])
@auth.login_required
def download_csv_file(csv_file_name):
    return send_from_directory(app.config['CSV_FOLDER'], filename=csv_file_name, as_attachment=True, attachment_filename="results-" + str(datetime.now()) + ".csv", cache_timeout=0)


@app.route('/cleanup', methods=['POST'])
@auth.login_required
def cleanup_files():
    if request.method == 'POST':
        cleanup(30, app.config['PHOTOS_DEST'])
        cleanup(30, app.config['CSV_FOLDER'])

    return "Cleanup Successful"


if __name__ == "__main__":
    # decide what port to run the app in
    port = int(os.environ.get('PORT', 5000))
    # run the app locally on the givn port
    app.run(threaded=True, host='0.0.0.0', port=port)
    # optional if we want to run in debugging mode
    # app.run(debug=True)
