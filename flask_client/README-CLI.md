# Prerequisites:

## Install AWS CLI

https://aws.amazon.com/cli/

## Install Docker

https://docs.docker.com/install/

## Configure AWS CLI

To configure, type:

aws configure

### Details to be provided:

aws_access_key_id = AKIA24SPQYVRQ2ANOWCQ

aws_secret_access_key = 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

region = ap-south-1

Leave the output format as [None]

# Instructions for using Docker image on ECR

# Logging into AWS ECR
sudo $(aws ecr get-login --no-include-email --region ap-south-1)

# Downloading Docker Image
sudo docker pull 748564825443.dkr.ecr.ap-south-1.amazonaws.com/iucn-image:latest

# Go to the directory with the images and run the following command

sudo docker run -v "$PWD":/base_dir -d -p 5000:5000 -t --name iucn-container 748564825443.dkr.ecr.ap-south-1.amazonaws.com/iucn-image:latest

## To run the prediction on the folder:

sudo docker exec -it iucn-container python /code/cli.py

If you think the prediction can go much faster on your machine configuration (based on the cpu fan speed),

sudo docker exec -it iucn-container python /code/cli.py --learner_batchsize=16

Note: Default learner_batchsize is 8. You can change the number appropriately, depending on the configuration.

# While the container is up, if you want to bring up the web application

sudo docker exec -it iucn-container /code/start_app.sh &

The application is available @ http://localhost:5000

# Cleanup
sudo docker container stop iucn-container
sudo docker container rm iucn-container