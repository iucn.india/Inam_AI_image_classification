import os
import uuid
from datetime import datetime, timedelta
from os.path import getsize, join
from pathlib import Path

import pandas as pd

from image_util import get_image_file_date_time, is_valid_image
from ml import predict_image_classes


def get_unique_image_file_path(image_file_path, target_dir):
    if not os.path.exists(target_dir):
        os.makedirs(target_dir)

    _, filename = os.path.split(image_file_path)
    _, extension = os.path.splitext(filename)
    new_unique_base = str(uuid.uuid4())
    new_file_name = new_unique_base + extension
    new_file_path = join(target_dir, new_file_name)
    return new_file_path, new_file_name


def get_unique_filename_with_extn(extn='.csv'):
    new_unique_base = str(uuid.uuid4())
    new_file_name = new_unique_base + extn
    return new_file_name


def get_image_info_list_from_folder(base_dir: str):
    file_info_list = []
    for root, _, files in os.walk(os.path.abspath(base_dir)):
        for img_file in files:
            img_file_path = join(root, img_file)
            if not is_valid_image(img_file_path):
                continue
            
            rel_path = str(Path(root).relative_to(base_dir))
            event_set_prefix = None if rel_path == '.' else rel_path

            file_info = {'event_set': str(root), 'event_set_prefix': event_set_prefix, 'img_file_path': img_file_path, 'photo_id': img_file, 'enc_file_name': img_file}
            file_info_list.append(file_info)
    return file_info_list


def __mark_event_beginnings(row):
    if row['previous_eventset'] != row['event_set']:
        return True

    time_diff = row['time_diff']
    if pd.isnull(time_diff):
        return True
    return True if time_diff and timedelta(minutes=1) > time_diff else None


def __generate_event_name(row):
    if not row.event_set_prefix:
        return row.short_event_name

    return row.event_set_prefix + ' :: ' + row.short_event_name


def add_event_metadata(df: pd.DataFrame):
    df['image_date_time'] = df['img_file_path'].apply(get_image_file_date_time)
    df.sort_values(['event_set', 'image_date_time'])
    df.reset_index(inplace=True, drop=True)

    df['previous_image_date_time'] = df['image_date_time'].shift()

    df['time_diff'] = df['image_date_time'] - df['previous_image_date_time']

    df['previous_eventset'] = df['event_set'].shift()

    df['evt_start'] = df.apply(__mark_event_beginnings, axis = 1)

    event_df = pd.DataFrame(df[df['evt_start'] == True]['evt_start'])
    event_df['event_index'] = range(1, len(event_df)+1)
    event_df['short_event_name'] = event_df.apply(
        lambda x: 'EVT' + str(x.event_index), axis=1)

    df['short_event_name'] = event_df['short_event_name']

    df['short_event_name'].fillna(method='ffill', inplace=True)
    df['event_name'] = df.apply(__generate_event_name, axis=1)


def add_rel_img_path(df: pd.DataFrame, target_dir):
    df['rel_img_file_path'] = df['img_file_path'].apply(
        lambda x: Path(x).relative_to(target_dir))


def convert_image_dataframe_to_results(image_dataframe, label_cols):

    results = dict()

    for _, row in image_dataframe.iterrows():
        event_name = row['event_name']
        img_path = row['enc_file_name']
        event_classes = row['event_classes']
        image_classes = row['image_classes']

        if event_name not in results:
            event_props = dict()
            event_props['event_classes'] = event_classes
            event_props['event_results'] = dict()
            results[event_name] = event_props

        event_results = results[event_name]['event_results']
        class_proba_dict = dict()
        for label_col in label_cols:
            class_proba_dict[label_col] = row[label_col]

        event_results[img_path] = {
            'class_proba': class_proba_dict, 'probable_classes': image_classes}

    return results


def process_image_dataframe(image_dataframe, learner_batchsize, ml_config, base_dir):
    add_rel_img_path(image_dataframe, base_dir)
    add_event_metadata(image_dataframe)
    image_dataframe = predict_image_classes(
        image_dataframe, learner_batchsize, ml_config, base_dir)
    return image_dataframe
