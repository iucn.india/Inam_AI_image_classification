import itertools
import operator
import os
from pathlib import Path

import pandas as pd
from fastai import *
from fastai.vision import (DatasetType, ImageDataBunch, ImageList,
                           get_transforms, load_learner)

ml_config = {
    "model_path": "./models/",
    "info": {
        "elephant": {
            "model_file": "elephant.pkl"
        },
        "big cats": {
            "model_file": "bigcats.pkl"
        },
        "person": {
            "model_file": "person.pkl"
        },
        "background": {
            "model_file": "background.pkl"
        },
        "other_animal": { 
            "model_file": "other_animal.pkl"
        }
    }
}


def save_as_csv(image_dataframe, csv_folder='./csv_result/', filename='result.csv'):
    if not os.path.exists(csv_folder):
        os.makedirs(csv_folder)
    df = image_dataframe[['photo_id', 'event_name',
                          'image_classes', 'event_classes']]
    df.to_csv(csv_folder + '/' + filename, index=False)


def __collect_image_classes(row, ml_config):
    image_map = dict()
    image_classes = []

    for label, model_info in ml_config['info'].items():
        image_map.update({label : row[label]})

    image_classes = max(image_map.items(), key = operator.itemgetter(1))[0]

    return image_classes


def __generate_event_image_classes(x):

    # event_classes = list(set(itertools.chain.from_iterable(x.tolist())))
    event_classes = list(set(itertools.chain(x.tolist())))

    if(len(event_classes) > 1 and 'background' in event_classes):
        event_classes.remove('background')
    return event_classes


def __get_event_classes_dict(image_dataframe):
    event_classes_df = image_dataframe.groupby(
        'event_name')['image_classes'].aggregate(__generate_event_image_classes)
    event_classes_dict = event_classes_df.to_dict()
    return event_classes_dict


def __get_pred_df(learner, label):
    predictions, *_ = learner.get_preds(DatasetType.Test)
    col_set = sorted([label, 'not_' + label])
    pred_df = pd.DataFrame(predictions, columns=col_set)
    pred_df = pred_df.applymap(lambda x: round(x.item() * 100, 2))
    return pred_df


def predict_image_classes(image_dataframe, learner_batchsize, ml_config, base_dir):

    only_single_image = False
    if len(image_dataframe) == 1:
        only_single_image = True
        image_dataframe = image_dataframe.append(image_dataframe)

    image_list = ImageList.from_df(
        image_dataframe, path=base_dir, cols='rel_img_file_path')

    model_path = ml_config['model_path']
    for label, model_info in ml_config['info'].items():
        model_file_name = model_info['model_file']
        learner = load_learner(path=Path(model_path),
                               file=model_file_name, test=image_list)
        learner.data.batch_size = learner_batchsize
        pred_df = __get_pred_df(learner, label)
        image_dataframe = image_dataframe.join(pred_df)

    if only_single_image:
        image_dataframe.reset_index(drop=True, inplace=True)
        image_dataframe = image_dataframe[image_dataframe.index == 0]

    image_dataframe['image_classes'] = image_dataframe.apply(
        lambda row: __collect_image_classes(row, ml_config), axis=1)

    event_classes_dict = __get_event_classes_dict(image_dataframe)
    image_dataframe['event_classes'] = image_dataframe['event_name'].apply(
        lambda x: event_classes_dict[x])
    return image_dataframe
