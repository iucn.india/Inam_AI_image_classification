# FastAI-Image-Classification-Flask-App

1) Download the repository to your local device 
2) In Terminal, cd to the folder where you have the repository downloaded
3) To install dependencies on your local machine, run pip install -r requirements.txt
4) Run python app.py


# Using Docker


## Building Docker Image with all necessary libraries

sudo docker build -t iucn-image .

## Running Docker container using the iucn-image:

sudo docker run -v "$PWD":/code -d -p 80:5000 -t --name iucn-container iucn-image

## Bash Terminal inside Docker container

sudo docker exec -it iucn-container /bin/bash

## Running application

sudo docker exec -it iucn-container /code/start_app.sh &

## Stop Container

sudo docker container stop iucn-container

## Remove Container
sudo docker container rm iucn-container

## Remove Image
sudo docker image rm iucn-image

# To Install Docker on Amazon EC2 with Amazon Linux 2

https://docs.aws.amazon.com/AmazonECS/latest/developerguide/docker-basics.html#install_docker

# Configure EC2 Security Group to open port 80

https://aws.amazon.com/premiumsupport/knowledge-center/connect-http-https-ec2/
