#!/bin/bash
DIRECTORY=$(cd `dirname $0` && pwd)

rm -rf env
virtualenv --python=python3 env
source env/bin/activate
pip install --no-cache-dir -r requirements.txt

mkdir -p ~/.jupyter
cp $DIRECTORY/conf/jupyter_notebook_config.py ~/.jupyter/jupyter_notebook_config.py