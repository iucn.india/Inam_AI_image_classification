About inamAI

Inam AI (“Inam” means species in Tamil) is an AI based application that enables classification of images into elephants, big cat, other animals, humans and background. This is the first version of the application.
It is specifically trained to classify images from camera traps and it is also capable of aggregating sets of images based on time into an event, and give a classification tag for that event.
The application can be used in two modes. One is in web-online mode (only designated users, authentication required), where the user can use to upload pictures and get the list of pictures classified. The other mode is in offline mode where the user can download the model and run the models as a command line.
This work was supported by IUCN as part of the European Union funded CITES-MIKE project ”Asia Wildlife Law Enforcement and Demand Management Project: MIKE Programme Sub-regional Support in South Asia and Southeast Asia”.
The application was developed by Zentropy Technologies Pvt Ltd.  

How were the models trained

Each of the classification was specifically trained with the pre-labelled corresponding images, and then tested both for in sample and out of sample images. It was then fine tuned (hyper parameter tunings) till we got the desired target accuracy level (in the high nineties). Since image classification training requires very high processing, GPUs were used during training.

Deployment model

Once the model training is done we download the trained model and host it on the production deployment server. We don’t need GPU for the prediction and the deployment can be done on a regular machine.

Command line deployment 

If the application needs to be run offline (as would be typical for those with a large number of camera trap images) it can be done using a command line option of the application. In this case, the application needs to downloaded to the local user machine and run within a docker runtime. Description provided in flask_client. 
