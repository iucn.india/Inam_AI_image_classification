# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.1.7
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

from fastai import *
from fastai.vision import *
from fastai.vision import image as im

np.random.seed(42)
data = ImageDataBunch.from_folder('fast_ai_20_samples/', train = '.', valid_pct = 0.2, ds_tfms = get_transforms(),size = 224, num_workers = 0, bs = 32).normalize(imagenet_stats)

data.classes

data.show_batch(rows=3,figsize=(7,8))

from fastai.metrics import error_rate # 1 - accuracy
learn = cnn_learner(data, models.resnet34, metrics=error_rate)

learn.fit_one_cycle(5)

interpret = ClassificationInterpretation.from_learner(learn)

doc(interpret.plot_top_losses)

interpret.plot_top_losses(6, figsize = (15,11))

doc(interpret.plot_confusion_matrix)

interpret.plot_confusion_matrix(figsize = (7,7))

#particular combination of predicted and actual get wrong
interpret.most_confused()

interpret.confusion_matrix()

doc(learn.unfreeze)

learn.save('stage-1')

learn.unfreeze()

# +

learn.fit_one_cycle(1)
# -

learn.load('stage-1')

learn.lr_find()

learn.recorder.plot()

learn.recorder.plot_lr()

img = im.open_image('flower-3140492_960_720.jpg')

pred_class, pred_idx, outputs = learn.predict(img)

pred_class

pred_idx

outputs


