# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.1.7
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

from fastai import *
from fastai.vision import *
from fastai.vision import image as im
import glob

np.random.seed(42)
data = ImageDataBunch.from_folder('fast_ai_dataset_500_samples/', train = '.', valid_pct = 0.2, ds_tfms = get_transforms(),size = 224, num_workers = 0, bs = 64).normalize(imagenet_stats)

data.classes

data.show_batch(rows=3,figsize=(7,8))

from fastai.metrics import error_rate # 1 - accuracy
learn = cnn_learner(data, models.resnet34, metrics=error_rate)

learn.fit_one_cycle(10)

interpret = ClassificationInterpretation.from_learner(learn)

interpret.plot_top_losses(9, figsize = (15,11))

interpret.plot_confusion_matrix(figsize = (7,7))

#particular combination of predicted and actual get wrong
interpret.most_confused()

learn.save('model_500_samples_1')

learn.load('model_500_samples_1')

if __name__ == '__main__':
    elephant_count = 0
    total_images = 0
    non_elephant_count = 0
    file_list = []
    for file in glob.glob('human/*'):
        file_list.append(file)
        if len(file_list) is 100:
            for i in file_list:
                if i.endswith(('.jpg','.JPG','.png','.jpeg')):
                    filename = i.split('/')[1]
                    img = im.open_image(i)
                    pred_class, pred_idx, outputs = learn.predict(img)
                    if(str(pred_class) is "elephant"):
                        elephant_count += 1
                    else:
                        non_elephant_count += 1
                    print("filename : {} \nresult : {} \n".format(filename,pred_class))
                total_images += 1
            file_list.clear()
    print('{} elephant images in {} total images'.format(elephant_count, total_images))


