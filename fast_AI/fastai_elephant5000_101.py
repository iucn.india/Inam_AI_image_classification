from fastai import *
from fastai.vision import *
from fastai.vision import image as im
import glob

np.random.seed(42)
data = ImageDataBunch.from_folder('fast_ai_elephant5000_sample/', train = '.', valid_pct = 0.2, ds_tfms = get_transforms(),size = 224, num_workers = 0, bs = 64).normalize(imagenet_stats)

data.classes

data.show_batch(rows=3,figsize=(7,8))

from fastai.metrics import error_rate # 1 - accuracy
learn = cnn_learner(data, models.resnet101, metrics=error_rate)

import time

start = time.time()
learn.fit_one_cycle(25)
end = time.time()
elapsed = end - start
print('elapsed time is : ', elapsed)

learn.save('elephant_resnet101_15000_25')

learn.export()

